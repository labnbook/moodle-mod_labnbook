<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @category    admin
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */
defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/lib.php');

if ($ADMIN->fulltree) {
    $settings->add(
        new admin_setting_configtext(
            'labnbook/api_url',
            get_string('labnbook_api_url', 'mod_labnbook'),
            get_string('labnbook_api_url_descr', 'mod_labnbook'),
            "",
            PARAM_URL
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'labnbook/api_key',
            get_string('labnbook_api_key', 'mod_labnbook'),
            get_string('labnbook_api_key_descr', 'mod_labnbook'),
            "",
            PARAM_ALPHANUM
        )
    );
    $settings->add(
        new admin_setting_configtext(
            'labnbook/institution_id',
            get_string('labnbook_institution_id', 'mod_labnbook'),
            get_string('labnbook_institution_id_descr', 'mod_labnbook'),
            "",
            PARAM_INT
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'labnbook/send_user_email',
            get_string('labnbook_send_user_email', 'mod_labnbook'),
            get_string('labnbook_send_user_email_descr', 'mod_labnbook'),
            '1'
        )
    );
    $settings->add(
        new admin_setting_configcheckbox(
            'labnbook/send_user_student_num',
            get_string('labnbook_send_user_student_num', 'mod_labnbook'),
            get_string('labnbook_send_user_student_num_descr', 'mod_labnbook'),
            '1'
        )
    );
}
