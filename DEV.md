Developing the Moodle module for LabNbook
=========================================

The core process is the communication between Moodle (M) and LabNbook (L).
It is handled by PHP classes in `classes/fetch`, notably `institution` and `user`.

Main files:

- `lib.php` implements the Moodle mod API,
  e.g. `labnbook_add_instance()` that creates a new activity in a course.
- `mod_form` is the form that creates and updates an activity.
- `db/` contains the DB install (XML) and upgrade process (PHP).


Adding a new instance
---------------------

Every request to the LabNbook API uses a token:

- an institution token (I), through the class `fetch\institution`,
- or a user token (U), through the class `fetch\user`.

```mermaid
sequenceDiagram
    participant H as Human (web browser)
    participant M as Moodle
    participant L as LabNbook API
    Note over H: Add new activity
    H->>M: /course/modedit.php
    activate M
    Note over M: require labnbook/mod_form.php
    M->>L: /v1/auth/login (I)
    alt user unknown
        L-->>M: HTTP 403
        M-->>H: redirect user-binding.php ➀
    else user found by M ID
        L-->>M: HTTP 200 user token
        M-->>H: HTML form
    end
    deactivate M

    activate H
    Note over H: display the form
    H-xL: AJAX /v1/mission (U)
    L--xH: list of allowed missions
    Note over H: user submits form
    deactivate H

    H->>M: /course/modedit.php
    activate M
    Note over M: require labnbook/mod_form.php
    Note over M: parses input
    Note over M: calls lib.php add_instance()
    M-->>H: HTTP 302 redirect ➁
    deactivate M
```

➀: see next sequence diagram

➁: The destination of the final redirection depends on the submit button
that was chosen.

The AJAX request is sent by the web browser,
but prepared by the PHP server using the (U) token.


Binding Moodle and LabNbook accounts
------------------------------------

In `mod/labnbook/`, the page `user-binding.php` tries to bind
a Moodle account with a LabNbook account.

If the binds succeeds, the user will be redirected to the Moodle page
that requested the bind.

```mermaid
sequenceDiagram
    participant H as Human (web browser)
    participant M as Moodle
    participant L as LabNbook
    H->>M: user-binding.php
    activate M
    M->>L: /v1/auth/login (I)
    alt user found by M ID
        L-->>M: HTTP 200 user token
        Note over M: stores (U) token
        M-->>H: redirect to Moodle
    else user unknown
        L-->>M: HTTP 403
        M-->>H: HTML form
    end
    deactivate M

    note over H: form with buttons
    alt existing account
        note over H: button Existing
        H->>L: /v1/redirect (I)
        L-->>H: redirect /user/bind
        H->>L: /user/bind
        L-->>H: HTML page
        Note over H: fills the form
        H->>L: submit
        L->>H: redirect to Moodle
    else new account
        note over H: button Create
        H->>M: user-binding.php?create
        M->>L: /v1/user/create (I)
        L-->>M: user token
        Note over M: stores (U) token
        M->>H: redirect to Moodle
    end
```

If the M user binds to an existing L account, Moodle won't receive the user token directly.
This token will be receined from L after the final redirection, when Moodle checks for authentication.

With the other cases, Moodle receives the user token before the final redirection,
so the user token is stored in the session, and the check for authentication
will not send a request to the L API.


Moodle documentation
--------------------

- <https://docs.moodle.org/dev/>
- [Official tutorial for a mod](https://docs.moodle.org/dev/Activity_modules)

Moodle conventions and code style are very different from PHP standards.

PHP classes must be in sub-directories of `classes/`,
see [Automatic class loading](Automatic class loading).

Texts should be inserted with `get_string(<stringid>, "labnbook")` so that translations are possible.
The string IDs are mapped to strings through the file `lang/en/labnbook.php`.
See [Strings API](https://docs.moodle.org/dev/String_API) (sic) for l10n.

### Upgrading

The process is the standard Moodle plugin process,
see <https://docs.moodle.org/dev/Activity_modules#upgrade.php> for a tutorial.

If the DB structure is modified, you must modify the XML structure *and*
write the PHP migration.

If necessary, update the DB structure *for new installs* in `db/install.xml`.
Editing the XML is possible, but the recommended way is to use the
[web interface in Moodle](https://docs.moodle.org/dev/XMLDB_editor).

Then modify `db/upgrade.php` to apply the changes to plugins already installed.

