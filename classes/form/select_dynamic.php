<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A select_dynamic base on MoodleQuickForm_select without filtering
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */

namespace mod_labnbook\form;

use is_array;
use is_null;
use MoodleQuickForm_select;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/form/select.php');

/**
 * select_dynamic differs from Moodle's select by not filtering the value.
 *
 * So this select options do not have to be declared in the PHP model,
 * one can add them dynamically.
 * @package mod_labnbook
 */
class select_dynamic extends MoodleQuickForm_select {
    /**
     * Overrides the filtering method.
     *
     * @param array $submitvalues submitted values
     * @param bool $assoc if true the returned value is an assoc array
     * @return string|array
     */
    public function exportvalue(&$submitvalues, $assoc = false) {
        if (empty($this->_options)) {
            return $this->_prepareValue(null, $assoc);
        }

        $value = $this->_findValue($submitvalues);
        if (is_null($value)) {
            $value = $this->getValue();
        }

        if ($this->getMultiple()) {
            return $this->_prepareValue((array) $value, $assoc);
        }
        return $this->_prepareValue(is_array($value) ? $value[0] : $value, $assoc);
    }
}
