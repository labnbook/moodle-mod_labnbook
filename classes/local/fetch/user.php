<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Low level fetcher implementation for all remote curl calls
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2019 Université Grenoble Alpes
 * @package mod_labnbook
 */

namespace mod_labnbook\local\fetch;

use Exception;
use time;
use moodle_exception;

/**
 * Send queries to the LabnBook API as an authenticated LnB user.
 * @package mod_labnbook
 */
class user extends moodlefetcher {
    /** @var int error when the token is expired or wrong */
    const ERROR_TOKEN_NOTVALID = 401;
    /** @var int error when the user is unknown */
    const ERROR_LOGIN_UNKNOWN = 403;
    /** @var int error the user creation failed */
    const ERROR_CREATION_FAILED = 500;

    /**
     * Check that the current user has a Labnbook authentication, valid for the next 30 s.
     */
    public static function isauthenticated() {
        global $SESSION;
        return !empty($SESSION->labnbook->auth->token_exp)
            && $SESSION->labnbook->auth->token_exp > time() + 30;
    }

    /**
     * Returns the teamconfig froms its id
     *
     * @param int $teamconfigid
     * @return object teamconfig
     */
    public function getteamconfig($teamconfigid) {
        return $this->fetch('GET', '/v1/teamconfig/' . $teamconfigid);
    }

    /**
     * Returns the number of started reports for the teamconfig
     *
     * @param int $teamconfigid
     * @return int
     */
    public function getnumstartedreports($teamconfigid) {
        return $this->fetchignore('GET', '/v1/teamconfig/' . $teamconfigid . '/countStartedReports', [], 0);
    }

    /**
     * Returns the number of submitted reports for the teamconfig
     *
     * @param int $teamconfigid
     * @return int
     */
    public function getnumsubmittedreports($teamconfigid) {
        return $this->fetchignore('GET', '/v1/teamconfig/' . $teamconfigid . '/countSubmittededReports', [], 0);
    }

    /**
     * Ensure that a user is a teacher in LabNBook.
     *
     */
    public function grantteacheraccess() {
        $this->fetch(
            'POST',
            "/v1/user/grantteacher",
            []
        );
    }

    /**
     * Use a LnB mission in a Moodle activity.
     *
     * @param int $missionid
     * @param int $courseid
     * @param string $coursename
     * @param int|null $groupid
     * @param array $participants
     * @param array $teamconfig
     * @return object teamconfig
     */
    public function usemission(
        int $missionid,
        int $courseid,
        string $coursename,
        ?int $groupid = null,
        array $participants = [],
        array $teamconfig = []
    ) {
        return $this
            ->fetch(
                'POST',
                "/v1/mission/$missionid/use",
                [
                    'courseid' => $courseid,
                    'coursename' => $coursename,
                    'groupid' => $groupid,
                    'participants' => $participants,
                    'team_config' => $teamconfig,
                ]
            )
            ->team_config;
    }

    /**
     * Update the participants of a Moodle-LnB activity.
     * @param int $courseid
     * @param int|null $groupid
     * @param array $participants
     * @return array
     */
    public function updateparticipants(int $courseid, ?int $groupid = null, array $participants = []) {
        return $this
            ->fetchignore(
                'POST',
                "/v1/classe/updateParticipants",
                [
                    'courseid' => $courseid,
                    'groupid' => $groupid,
                    'participants' => $participants,
                ],
                []
            );
    }



    /**
     * Return the token to fetches the missions.
     *
     * E.g. <?= $u->getjsmissions() ?>.then(missions => for (m of missions) {...})
     *
     * @return array [url, token]
     */
    public function getmissionstoken() {
        $path = '/v1/mission';
        return [$this->fetcher->createurl($path), $this->fetcher->gettoken('/v1/mission')];
    }

    /**
     * Return a JWT token for the redirection page of LnB, pointing to this report page.
     *
     * @param int $teamconfigid
     * @return string JWT
     * @throws moodle_exception
     */
    public function getreporttokenforredirect(int $teamconfigid): string {
        $payload = [
            'path' => "/report/enter/$teamconfigid",
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Return a JWT token for the redirection page of LnB, pointing to the table of reports.
     *
     * @param int $teamconfigid
     * @return string JWT
     * @throws moodle_exception
     */
    public function getreportstokenforredirect(int $teamconfigid): string {
        $payload = [
            'path' => "/teacher/reports/?team_config=$teamconfigid",
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Return a JWT token for the redirection page of LnB, pointing to mission edition page.
     *
     * @param int $missionid
     * @return string JWT
     * @throws moodle_exception
     */
    public function getmissionedittokenforredirect(int $missionid): string {
        $payload = [
            'path' => "/teacher/mission/$missionid",
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Return a JWT token for the redirection page of LNB pointing to class edition page
     *
     * @param int $idteamconfig
     * @return string JWT
     * @throws moodle_exception
     */
    public function getclassedittokenforredirect(int $idteamconfig): string {
        $payload = [
            'path' => "/teacher/students?id_team_config=" . $idteamconfig,
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Return a JWT token for the redirection page of LnB, pointing to the config of this teaming.
     *
     * @param int $teamconfigid
     * @return string JWT
     * @throws moodle_exception
     */
    public function getteamingtokenforredirect(int $teamconfigid): string {
        $payload = [
            'path' => "/teacher/students#teamconfig=$teamconfigid",
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Return a JWT token for the redirection page of LnB, point to the page that lists missions.
     *
     * @return string JWT
     * @throws moodle_exception
     */
    public function getmissiontokenforredirect(): string {
        $payload = [
            'path' => "/teacher/missions",
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Returns the environment for fetching labnbook i.e orig and token expiration
     * @return environment
     */
    protected function getenvironment(): environment {
        global $SESSION;
        if (empty($SESSION->labnbook->auth->token_exp)) {
            throw new Exception("User token for the Labnbook API does not exist tokenexp.");
        }
        $env = parent::getenvironment();
        $env->orig = "user";
        $env->tokenexp = $SESSION->labnbook->auth->token_exp;
        return $env;
    }

    /**
     * Returns the signing key for the request
     * @return string
     */
    protected function getsigningkey(): string {
        global $SESSION;
        if (empty($SESSION->labnbook->auth->token)) {
            throw new Exception("User token for the Labnbook API does not exist toekn.");
        }
        return $SESSION->labnbook->auth->token;
    }
}
