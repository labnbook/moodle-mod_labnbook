<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Fetch info from the LabNbook API.
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2019 Université Grenoble Alpes
 * @package mod_labnbook
 */

namespace mod_labnbook\local\fetch;

use Exception;
use stdClass;
use json_decode;
use json_encode;
use ltrim;
use rtrim;
use time;
use trim;
use Firebase\JWT\JWT;
use curl;

/**
 * Fetch info from the LabNbook API.
 *
 * This class does not depend on the environment (Moodle user, config, role, etc).
 * @package mod_labnbook
 */
class fetcher {
    /**
     * @var int $TIMEOUThow long before timing out in seconds
     */
    const TIMEOUT = 2;

    /**
     * @var string Root URL to the API
     */
    private $baseurl;

    /**
     * @var string Secret key for signing JWT
     */
    private $secret;

    /**
     * @var environment base of the JWT payload across requests
     */
    private $environment;

    /**
     * @var stdClass { code, message }, e.g. { code: 403, message: "not auth" }
     */
    private $lasterror;

    /**
     * Constructor
     * @param string $baseurl
     * @param string $secret
     * @param environment $environment
     */
    public function __construct(string $baseurl, string $secret, environment $environment) {
        if (!$secret) {
            throw new Exception("The JWT secret key is empty.");
        }
        $this->baseurl = rtrim($baseurl, '/');
        $this->secret = $secret;
        $this->environment = $environment;
    }

    /**
     * Return the last error
     *
     * @return stdClass {code, message}
     */
    public function getlasterror(): ?stdClass {
        return (object) $this->lasterror;
    }

    /**
     * Query the LabNbook API.
     *
     * @param string $verb E.g. "GET"
     * @param string $urlpath E.g. "/v1/mission"
     * @param array $payload Default is empty. Else any assoc array.
     * @return mixed
     */
    public function fetch(string $verb, string $urlpath, array $payload = []) {
        if (!in_array($verb, ['GET', 'POST'])) {
            throw new Exception("Forbidden verb {$verb}");
        }
        $method = strtolower($verb);
        $curl = new curl();
        $curl->setopt([
            'CURLOPT_RETURNTRANSFER' => true,
            'CURLOPT_CONNECTTIMEOUT' => self::TIMEOUT,
        ]);
        $curl->setHeader([
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer ' . $this->encodeJwtToken($urlpath, $payload),
        ]);
        $response = $curl->$method($this->createurl($urlpath));
        if ($curl->get_errno() !== 0) {
            $this->lasterror = (object) [
                'code' => $curl->get_info()['http_code'],
                'message' => $curl->error,
            ];
        }
        $error = $this->validateResponse($curl, $response);
        if ($error) {
            throw new Exception("HTTP request failed.");
        }

        // Send decoded response?
        $contenttype = $curl->get_info()['content_type'];
        if ($contenttype === 'application/json') {
            return json_decode($response);
        }
        return $response;
    }

    /**
     * Generate an url from a path
     * @param string $urlpath
     * @return string
     */
    public function createurl(string $urlpath): string {
        return $this->baseurl . '/' . ltrim($urlpath, '/');
    }

    /**
     * the token for running a fetch
     *
     * @param string $urlpath E.g. "/v1/mission"
     * @param array $payload Default is empty. Else any assoc array.
     * @return string JS source code
     */
    public function gettoken(string $urlpath, array $payload = []) {
        $url = json_encode($this->createurl($urlpath));
        return $this->encodeJwtToken($urlpath, $payload);
    }

    /**
     * Encodes the payload in a JWT token
     * @param string $urlpath
     * @param array $payload
     * @return array
     */
    public function encodejwttoken(string $urlpath, array $payload): string {
        $jwtpayload = $this->environment->createpayload();
        if ($payload) {
            $jwtpayload['data'] = $payload;
        }
        $jwtpayload['dest'] = '/' . trim($urlpath, '/');
        $jwtpayload['iat'] = time(); // UTC timestamp.
        return JWT::encode($jwtpayload, $this->secret, 'HS256');
    }

    /**
     * Checks that the response is OK
     *
     * @param \curl $curl
     * @param string $response
     * @return string if OK "", else error message
     */
    protected function validateresponse($curl, $response) {
        if ($response === false) {
            $this->lasterror = (object) ['message' => "Network error? " . $curl->error];
            return "Network error? " . $curl->error;
        }
        $responsecode = $curl->get_info()['http_code'];
        if ($responsecode != 200) {
            $this->lasterror = json_decode($response); // Do not verify signature?
            if (json_last_error() !== JSON_ERROR_NONE) {
                $this->lasterror = (object) ['message' => $response];
            }
            $this->lasterror->code = $responsecode;
            return "HTTP code $responsecode, response: " . $response;
        }
        return "";
    }
}
