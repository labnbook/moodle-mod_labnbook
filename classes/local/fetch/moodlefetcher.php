<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Integrate Moodle needs to the composed fetcher.
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2019 Université Grenoble Alpes
 * @package mod_labnbook
 */

namespace mod_labnbook\local\fetch;

use Exception;
use rtrim;
use stdClass;

/**
 * Integrate Moodle needs to the composed fetcher.
 *
 * It fills data and actions for the communication with LabnBook through a fetcher.
 * It will be extended by 2 classes, for exchanges as a LnB institution or a LnB user.
 * @package mod_labnbook
 */
abstract class moodlefetcher {
    /**
     * @var fetcher
     */
    protected $fetcher;

    /**
     * Constructor
     */
    public function __construct() {
        $this->fetcher = new fetcher(
            get_config('labnbook', 'api_url'),
            $this->getsigningkey(),
            $this->getenvironment()
        );
    }

    /**
     * Returns a redirection url
     * @return string
     */
    public function getredirecturl(): string {
        return rtrim(get_config('labnbook', 'api_url'), '/') . '/v1/redirect';
    }

    /**
     * Returns the last error
     * @return stdClass|null
     */
    public function getlasterror(): ?stdClass {
        return $this->fetcher->getlasterror();
    }

    /**
     * Returns the fetch environament
     *
     * @return environment
     */
    protected function getenvironment(): environment {
        global $USER;
        $env = new environment();
        // Default value.
        $env->orig = "inst";
        $env->iss = (int) get_config('labnbook', 'institution_id');
        $env->sub = (int) $USER->id;
        return $env;
    }

    /**
     * Runs an API call
     * @param string $verb GET|POST
     * @param string $path
     * @param array $payload
     * @return mixed
     */
    protected function fetch($verb, $path, $payload = []) {
        global $SESSION;
        $response = $this->fetcher->fetch($verb, $path, $payload);
        if (!empty($response->auth)) {
            if (empty($SESSION->labnbook)) {
                $SESSION->labnbook = new stdClass();
            }
            $SESSION->labnbook->auth = $response->auth;
        }
        return isset($response->data) ? $response->data : $response;
    }

    /**
     * Query the LabNbook API, returns default value in case of error
     *
     * @param string $verb E.g. "GET"
     * @param string $urlpath E.g. "/v1/mission"
     * @param array $payload Default is empty. Else any assoc array.
     * @param mixed $default value to return in case of errors
     * @return mixed
     */
    protected function fetchignore(string $verb, string $urlpath, array $payload = [], $default = null) {
        try {
            return $this->fetch($verb, $urlpath, $payload);
        } catch (Exception $e) {
            return $default;
        }
    }

    /**
     * Returns the signing key for the context
     * @return string
     */
    abstract protected function getsigningkey(): string;
}
