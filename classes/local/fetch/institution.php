<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Send queries to the LabnBook API as an authenticated LnB institution.
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2019 Université Grenoble Alpes
 * @package mod_labnbook
 */

namespace mod_labnbook\local\fetch;

use Exception;
use moodle_exception;

/**
 * Send queries to the LabnBook API as an authenticated LnB institution.
 * @package mod_labnbook
 */
class institution extends moodlefetcher {
    /** @var int status when everything is ok */
    const OK = 200;
    /** @var int status on network error */
    const ERROR_NETWORK = 0;
    /** @var int status on invalid token */
    const ERROR_TOKEN_NOTVALID = 401;
    /** @var int status on unknown user */
    const ERROR_LOGIN_UNKNOWN = 403;
    /** @var int status on creation error */
    const ERROR_CREATION_FAILED = 500;

    /**
     * Authenticates a user
     *
     * @return int Cf constants: OK, ERROR_*
     */
    public function loginuser(): int {
        try {
            $this->updateinfo();
            $this->fetch("POST", "/v1/auth/login");
        } catch (Exception $e) {
            $error = $this->fetcher->getlasterror();
            return (isset($error->code) ? (int) $error->code : 0);
        }
        return self::OK;
    }

    /**
     * Sends info to LabNbook
     */
    public function updateinfo() {
        $data = [
            'plugin_version' => get_config('mod_labnbook')->version,
            'platform_version' => 'Moodle ' . get_config('moodle')->release,
        ];
        $this->fetch("POST", "/v1/extplatform", $data);
    }

    /**
     * Return a JWT token for the redirection page of LnB, so that the user ends on the user-binding page.
     *
     * @param string $returnurl full URL, including protocol
     * @param string $role "learner" | "teacher"
     * @param stdClass $user
     * @return string JWT
     * @throws moodle_exception
     */
    public function getbindingtokenforredirect(string $returnurl, string $role, $user): string {
        if ($role !== 'learner' && $role !== 'teacher') {
            throw new moodle_exception("Invalid role value in fetch.institution.getBindingToken()");
        }
        $email = get_config('labnbook', 'send_user_email') ? $user->email : null;
        $instnumber = get_config('labnbook', 'send_user_student_num') ? $user->idnumber : null;
        $payload = [
            'path' => "/user/preBind",
            'forward' => [
                'role_ext' => $role,
                'return_to' => $returnurl,
                'login' => $user->username,
                'user_name' => $user->lastname,
                'first_name' => $user->firstname,
                'inst_number' => $instnumber,
                'email' => $email,
            ],
        ];
        return $this->fetcher->encodeJwtToken('/v1/redirect', $payload);
    }

    /**
     * Returns the signing key for the institution
     * @return string
     */
    protected function getsigningkey(): string {
        return get_config('labnbook', 'api_key');
    }

    /**
     * deletes a teamconfig
     * @param int $teamconfigid
     * @return boolean
     */
    public function deleteteamconfig($teamconfigid) {
        return $this->fetch('POST', '/v1/teamconfig/' . $teamconfigid . '/delete');
    }

    /**
     * Get a RGPD dump for a given user
     * @param int $userid
     * @return array
     */
    public function exportuserdata($userid) {
        $payload = [
            'user' => $userid,
        ];
        return $this->fetch('GET', '/v1/user/exportData', $payload);
    }


    /**
     * Get a RGPD dump for a given user
     * @param array $teamconfigids
     * @return boolean
     */
    public function getuserforteamconfigs($teamconfigids) {
        $payload = [
            'teamconfigids' => $teamconfigids,
        ];
        return $this->fetch('GET', '/v1/user/fromTeamConfigIds', $payload);
    }


    /**
     * Get a RGPD dump for a given user
     * @param int $userid
     * @param array $teamconfigids
     * @return boolean
     */
    public function deleteuserdata($userid, $teamconfigids) {
        $payload = [
            'user' => $userid,
            'teamconfigids' => $teamconfigids,
        ];
        return $this->fetch('POST', '/v1/user/deleteData', $payload);
    }
}
