<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Environment of a fetcher, which will produce the base for the JWT payload.
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2019 Université Grenoble Alpes
 * @package mod_labnbook
 */

namespace mod_labnbook\local\fetch;

/**
 * Environment of a fetcher, which will produce the base for the JWT payload.
 *
 * @package mod_labnbook
 */
class environment {
    /**
     * @var string "inst"|"user"
     */
    public $orig;

    /**
     * @var int issuer (institution ID)
     */
    public $iss;

    /**
     * @var int subject (user ID)
     */
    public $sub;

    /**
     * @var string Expiration timestamp, as received from the server
     */
    public $tokenexp;

    /**
     * Creates a payload
     * @return array
     */
    public function createpayload(): array {
        $payload = [
            'iss' => (int) $this->iss,
            'sub' => (int) $this->sub,
            'orig' => (string) $this->orig,
        ];
        if ($this->tokenexp) {
            $payload['token_exp'] = (int) $this->tokenexp;
        }
        return $payload;
    }
}
