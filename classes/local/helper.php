<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A helper class with Misc functions.
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */

namespace mod_labnbook\local;

use base64_encode;
use mod_labnbook\local\fetch;
use mod_labnbook\local\fetch\user as fetchUser;
use moodle_exception;

/**
 * Misc functions.
 *
 * @package mod_labnbook
 */
class helper {
    /** @var string LOCALURLPREFIX a prefix on url to indicate it should be replaced by a labnbook remote url */
    const LOCALURLPREFIX = 'lnb://';

    /**
     * Stop loading the current page if the current user has no LabnBook account.
     * Redirects on an account binding|creation if necessary.
     *
     * @throws moodle_exception
     * @param string $destination where to return in case of error
     */
    public static function requirelabnbookauthentication($destination = null) {
        global $CFG, $COURSE;
        if (fetch\user::isauthenticated()) {
            return;
        }
        $institutionfetcher = new fetch\institution();
        switch ($institutionfetcher->loginuser()) {
            case fetch\institution::OK:
                return;
            case fetch\institution::ERROR_LOGIN_UNKNOWN:
                if (!$destination) {
                    $destination = $_SERVER['REQUEST_URI'];
                }
                $id = optional_param('id', 0, PARAM_INT);
                redirect(
                    $CFG->wwwroot . "/mod/labnbook/user-binding.php?courseid={$COURSE->id}&id={$id}&return="
                    . base64_encode($destination)
                );
                break;
            case fetch\institution::ERROR_TOKEN_NOTVALID:
                throw new moodle_exception("Internal error with LabNbook");
            case fetch\institution::ERROR_NETWORK:
                throw new moodle_exception("Unable to connected to LabNbook, network error");
            default:
                throw new moodle_exception("Unknown error with LabNbook");
        }
    }

    /**
     * Grant teacher access to the LnB missions and classes linked to the course.
     *
     */
    public static function grantteacheraccess() {
        global $DB, $SESSION;
        if ($SESSION->labnbook->grantteacher) {
            return;
        }
        if (!fetch\user::isauthenticated()) {
            return;
        }
        $userfetcher = new fetch\user();
        $userfetcher->grantteacheraccess();
        // Use session to avoid repeating this query.
        $SESSION->labnbook->grantteacher = true;
    }

    /**
     * Get the local url to redirect for the activity
     * @param stdClass $moduleinstance
     * @param boolean $isteacher
     * @return string
     */
    public static function getactivitylocalurlforredirect($moduleinstance, $isteacher) {
        $url = $isteacher ? "/teacher/reports/?team_config=" : "/report/enter/";
        return self::LOCALURLPREFIX . $url . $moduleinstance->labnbook_teamconfigid;
    }
}
