<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines {@see \mod_labnbook\privacy\provider} class.
 *
 * @category    privacy
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */

namespace mod_labnbook\privacy;

use mod_labnbook\local\fetch;
use core_privacy\local\metadata\collection;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\transform;
use core_privacy\local\request\userlist;
use core_privacy\local\request\writer;

/**
 * Privacy API implementation for the LabNbook plugin.
 * @package mod_labnbook
 */
class provider implements
    // This plugin has data.
    \core_privacy\local\metadata\provider,

    // This plugin currently implements the original plugin\provider interface.
    \core_privacy\local\request\plugin\provider,

    // This plugin is capable of determining which users have data within it.
    \core_privacy\local\request\core_userlist_provider {
    /**
     * Returns metadata.
     *
     * @param collection $collection The initialised collection to add items to.
     * @return collection A listing of user data stored through this system.
     */
    public static function get_metadata(collection $collection): collection {

        // Personal information has to be passed to LabNBook.
        $data = [
                'username' => 'privacy:metadata:labnbook:username',
                'firstname' => 'privacy:metadata:labnbook:firstname',
                'lastname' => 'privacy:metadata:labnbook:lastname',
        ];
        if (get_config('labnbook', 'send_user_email')) {
            $data['email'] = 'privacy:metadata:labnbook:email';
        }
        if (get_config('labnbook', 'send_user_student_num')) {
            $data['idnumber'] = 'privacy:metadata:labnbook:idnumber';
        }
        // This includes the user ID and fullname.
        $collection->add_external_location_link('labnbook', $data, 'privacy:metadata:labnbook');

        return $collection;
    }
    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param   int           $userid       The user to search.
     * @return  contextlist   $contextlist  The list of contexts used in this plugin.
     */
    public static function get_contexts_for_userid(int $userid): contextlist {
        // If user was already deleted, do nothing.
        if (!\core_user::get_user($userid)) {
            return new contextlist();
        }
        // Fetch all labnbook activity.
        $sql = "SELECT c.id
                  FROM {context} c
            INNER JOIN {course_modules} cm
                    ON cm.id = c.instanceid
                   AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m
                    ON m.id = cm.module
                   AND m.name = :modname
            INNER JOIN {labnbook} labnbook
                    ON labnbook.course = cm.course
            INNER JOIN {enrol} e
                    ON cm.course = e.courseid
            INNER JOIN {user_enrolments} ue
                    ON ue.userid = :userid
                    AND ue.enrolid = e.id
            ";

        $params = [
            'modname' => 'labnbook',
            'contextlevel' => CONTEXT_MODULE,
            'userid' => $userid,
        ];
        $contextlist = new contextlist();
        $contextlist->add_from_sql($sql, $params);
        return $contextlist;
    }

    /**
     * Return a dict of labnbook IDs mapped to their course module ID.
     *
     * @param array $cmids The course module IDs.
     * @return array In the form of [$bigbluebuttonbnid => $cmid].
     */
    protected static function get_teamconfig_ids_to_cmids_from_cmids(array $cmids) {
        global $DB;

        list($insql, $inparams) = $DB->get_in_or_equal($cmids, SQL_PARAMS_NAMED);
        $sql = "SELECT labnbook.labnbook_teamconfigid, cm.id AS cmid
                 FROM {labnbook} labnbook
                 JOIN {modules} m
                   ON m.name = :labnbook
                 JOIN {course_modules} cm
                   ON cm.instance = labnbook.id
                  AND cm.module = m.id
                WHERE cm.id $insql";
        $params = array_merge($inparams, ['labnbook' => 'labnbook']);

        return $DB->get_records_sql_menu($sql, $params);
    }


    /**
     * Return an array of teamconfig ids to CMids.
     *
     * @param  approved_contextlist   $contextlist  The list of contexts used in this plugin.
     * @return array
     */
    protected static function get_teamconfigs_from_contextlist($contextlist) {
        // Filter out any contexts that are not related to modules.
        $cmids = array_reduce($contextlist->get_contexts(), function ($carry, $context) {
            if ($context->contextlevel == CONTEXT_MODULE) {
                $carry[] = $context->instanceid;
            }
            return $carry;
        }, []);

        if (empty($cmids)) {
            return [];
        }
        return self::get_teamconfig_ids_to_cmids_from_cmids($cmids);
    }

    /**
     * Export personal data for the given approved_contextlist. User and context information is contained within the contextlist.
     *
     * @param approved_contextlist $contextlist a list of contexts approved for export.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        $user = $contextlist->get_user();

        $teamconfigidstocmids = self::get_teamconfigs_from_contextlist($contextlist);

        $fetchinstitution = new fetch\institution();
        $lnbdump = $fetchinstitution->exportuserdata($user->id);

        foreach ($contextlist->get_contexts() as $context) {
            if ($context->contextlevel != CONTEXT_MODULE) {
                continue;
            }
            $contentwriter = writer::with_context($context);
            $teamconfigids = [];
            foreach ($teamconfigidstocmids as $tid => $cmid) {
                if ($cmid === $context->instanceid) {
                    $teamconfigids[] = $tid;
                }
            }
            foreach ($teamconfigids as $tid) {
                $reports = [];
                $reportsids = [];
                foreach ($lnbdump->Reports as $r) {
                    if ($r->id_team_config == $tid) {
                        $reports[] = $r;
                        $reportsids[] = $r->id_report;
                    }
                }
                $contentwriter->export_data([get_string('privacy:reports', 'labnbook')], (object) $reports);

                $labdocs = [];
                $labdocsids = [];
                foreach ($lnbdump->Labdocs as $ld) {
                    if (in_array($ld->id_report, $reportsids)) {
                        $labdocs[] = $ld;
                        $labdocsids[] = $ld->id_labdoc;
                    }
                }
                $contentwriter->export_data([get_string('privacy:labdocs', 'labnbook')], (object) $labdocs);

                $comments = [];
                foreach ($lnbdump->Comments as $com) {
                    if (in_array($com->id_labdoc, $labdocsids)) {
                        $comments[] = $com;
                    }
                }
                $contentwriter->export_data([get_string('privacy:comments', 'labnbook')], (object) $comments);

                $conversations = [];
                $conversationsids = [];
                foreach ($lnbdump->Conversations as $conv) {
                    if (in_array($conv->id_report, $reportsids)) {
                        $conversations[] = $conv;
                        $conversationsids[] = $conv->id_conversation;
                    }
                }
                $contentwriter->export_data([get_string('privacy:conversations', 'labnbook')], (object) $conversations);

                $messages = [];
                foreach ($lnbdump->Messages as $msg) {
                    if (in_array($msg->id_conversation, $conversationsids)) {
                        $mesages[] = $msg;
                    }
                }
                $contentwriter->export_data([get_string('privacy:messages', 'labnbook')], (object) $messages);
            }
        }
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param \context $context the context to delete in.
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        if (!$context instanceof \context_module) {
            return;
        }
        $cmids = [$context->instanceof];
        $teamconfigidstocmids = self::get_teamconfig_ids_to_cmids_from_cmids($cmids);
        $teamconfigids = array_keys($teamconfigidstocmids);
        $fetchinstitution = new fetch\institution();
        $lnbusers = $fetchinstitution->getuserforteamconfigs($teamconfigids);
        foreach ($lnbusers as $u) {
            $fetchinstitution->deleteuserdata($u->id, $teamconfigids);
        }
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist a list of contexts approved for deletion.
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        $userid = $contextlist->get_user()->id;
        $teamconfigids = array_keys(self::get_teamconfigs_from_contextlist($contextlist));
        $fetchinstitution = new fetch\institution();
        $fetchinstitution->deleteuserdata($userid, $teamconfigids);
    }


    /**
     * Get the list of users who have data within a context.
     *
     * @param userlist $userlist The userlist containing the list of users who have data in this context/plugin combination.
     */
    public static function get_users_in_context(\core_privacy\local\request\userlist $userlist) {
        global $DB;
        $context = $userlist->get_context();

        $sql = "SELECT labnbook.labnbook_teamconfigid, cm.id AS cmid
                 FROM {labnbook} labnbook
                 JOIN {modules} m
                   ON m.name = :labnbook
                 JOIN {course_modules} cm
                   ON cm.instance = labnbook.id
                  AND cm.module = m.id
                WHERE cm.id = :cmid";
        $params = array_merge(['labnbook' => 'labnbook', 'cmid' => $context->instanceid]);

        $teamconfigids = $DB->get_records_sql_menu($sql, $params);
        $fetchinstitution = new fetch\institution();
        $lnbusers = $fetchinstitution->getuserforteamconfigs($teamconfigids);
        $userlist->add_users($lnbusers);
    }


    /**
     * Delete multiple users within a single context.
     *
     * @param   approved_userlist       $userlist The approved context and user information to delete information for.
     */
    public static function delete_data_for_users(\core_privacy\local\request\approved_userlist $userlist) {
        global $DB;
        $context = $userlist->get_context();
        $cm = $DB->get_record('course_modules', ['id' => $context->instanceid]);
        $users = $userlist->get_userids();
        $fetchinstitution = new fetch\institution();
        $teamconfigidstocmids = self::get_teamconfig_ids_to_cmids_from_cmids([$context->instanceid]);
        $teamconfigids = array_keys($teamconfigidstocmids);
        foreach ($users as $u) {
            $fetchinstitution->deleteuserdata($u->id, $teamconfigids);
        }
    }
}
