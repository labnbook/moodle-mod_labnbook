<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A helper class with Misc functions.
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */

namespace mod_labnbook\external;

defined('MOODLE_INTERNAL') || die();
require_once("{$CFG->libdir}/externallib.php");

use mod_labnbook\local\fetch\user as fetchUser;
use mod_labnbook\local\helper;

/**
 * Checks if an activity has started reports on LabNbook.
 * @package mod_labnbook
 */
class activity_has_started_reports extends \external_api {
    /**
     * Returns description of method parameters
     * @return \external_function_parameters
     */
    public static function execute_parameters() {
        return new \external_function_parameters([
            'activityid' => new \external_value(PARAM_INT, 'id of activity'),
        ]);
    }

    /**
     * Returns description of method returns
     * @return \external_value
     */
    public static function execute_returns() {
        return new \external_value(PARAM_BOOL, 'are there any started reports');
    }

    /**
     * Checks if an activity has started reports
     * @param int $activityid
     * @return boolean
     */
    public static function execute($activityid) {
        global $CFG, $DB;
        require_once(__DIR__ . '/../../lib.php');
        require_once($CFG->libdir . '/filelib.php');
        $cm  = get_coursemodule_from_id("labnbook", $activityid, 0, false, MUST_EXIST);
        $course = $DB->get_record('course', ['id' => $cm->course], '*', MUST_EXIST);
        $moduleinstance = $DB->get_record(LABNBOOK_TABLE, ['id' => $cm->instance], '*', MUST_EXIST);
        helper::requireLabnbookAuthentication();
        $fetcher = new fetchUser();
        return $fetcher->getnumstartedreports($moduleinstance->labnbook_teamconfigid) > 0;
    }
}
