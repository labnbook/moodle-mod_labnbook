<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Inner HTL page - binds a Moodle user to a LabNbook account.
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */
use mod_labnbook\local\fetch;
use mod_labnbook\local\helper;

require(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/lib.php');

$return = required_param('return', PARAM_RAW);
$returnurl = base64_decode($return);
$courseid = required_param('courseid', PARAM_INT);
$id = required_param('id', PARAM_INT);

if ($id) {
    $cm             = get_coursemodule_from_id("labnbook", $id, 0, false, MUST_EXIST);
    $course         = $DB->get_record('course', ['id' => $cm->course], '*', MUST_EXIST);
    $moduleinstance = $DB->get_record(LABNBOOK_TABLE, ['id' => $cm->instance], '*', MUST_EXIST);
    unset($id);

    $modulecontext = context_module::instance($cm->id);
    $title = $moduleinstance->name;
} else {
    $course = $DB->get_record('course', ['id' => $courseid], '*', MUST_EXIST);
    $modulecontext = context_course::instance($courseid);
    $title = $course->fullname;
    $cm = null;
}
require_login($course, true, $cm);
$PAGE->set_url('/mod/labnbook/user_binding.php', ['courseid' => $courseid, 'return' => $return]);
$PAGE->set_title(format_string($title));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($modulecontext);

$fetchinstitution = new fetch\institution();
$context = context_course::instance($courseid);
$isteacher = has_capability('mod/labnbook:grade', $context);
$role = ($isteacher ? 'teacher' : 'learner');
global $USER;
$url = $fetchinstitution->getredirecturl();
if (false !== strpos($returnurl, helper::LOCALURLPREFIX)) {
    $actuallink = str_replace(helper::LOCALURLPREFIX, '', $returnurl);
} else {
    $actuallink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$returnurl";
}
$token = $fetchinstitution->getbindingtokenforredirect($actuallink, $role, $USER);

echo $OUTPUT->header();
$templatedata = [
    "url" => $url . "?token=" . $token,
    "send_email" => get_config('labnbook', 'send_user_email'),
    "send_student_num" => get_config('labnbook', 'send_user_student_num'),
];
echo $OUTPUT->render_from_template("mod_labnbook/view_bind", $templatedata);
echo $OUTPUT->footer();
