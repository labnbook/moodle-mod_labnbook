<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main mod_labnbook configuration form.
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */
use mod_labnbook\local\fetch;
use mod_labnbook\form\select_dynamic;
use mod_labnbook\local\helper;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/moodleform_mod.php');

// Short-cut before loading the form, see `course/modedit.php`.
helper::requireLabnbookAuthentication();
helper::grantteacheraccess();

/**
 * Module instance settings form.
 *
 * @copyright  2019 Université Grenoble Alpes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */
class mod_labnbook_mod_form extends moodleform_mod {
    /**
     * Defines forms elements.
     */
    public function definition() {
        global $CFG, $PAGE;

        $mform = $this->_form;
        $creationmode = $this->get_coursemodule() == null;
        $disabled = $creationmode ? [] : ["readonly" => "readonly"];

        $mform->addElement('header', 'external', get_string('labnbookSettings', 'mod_labnbook'));

        $fetcher = new fetch\User();
        if ($creationmode) {
            $createmissionurl = $fetcher->getredirecturl() . "?token=" . $fetcher->getmissiontokenforredirect();
            $mform->addElement(
                'static',
                'addMission',
                '',
                '<a href="#" onclick="window.open(\'' . $createmissionurl . '\', \'_blank\')" >'
                    . '<button type="button">'
                    . get_string('addMission', 'mod_labnbook')
                    . '</button></a>'
            );
            $mform->addHelpButton('addMission', 'addMission', 'mod_labnbook');
            $mform->addElement(
                'static',
                'refresh',
                '',
                '<button type="button" onclick="refreshMissions()" id="refresh-missions">'
                    . get_string('refresh', 'mod_labnbook')
                    . '</a>'
            );
            $mform->addHelpButton('refresh', 'refresh', 'mod_labnbook');
        }

        $attrs = $disabled;
        $attrs['onchange'] = 'javascript: '
            . 'document.querySelector("#id_name").value =  "["+getMission(this.value).code + "] " '
            . ' + getMission(this.value).name;'
                    // Update the Atto text, but Atto is undocumented stuff (wiki pages are all dead).
                    . 'document.querySelector("#id_introeditoreditable").innerHTML = getMission(this.value).description;'
                    . 'document.querySelector("#id_introeditor").innerHTML = getMission(this.value).description;';
                    // Todo update the TinyMCE text. Is there a way to detect the editor plugin? A common API for pluggable editors?

        $dynamicselect = new select_dynamic(
            'labnbook_missionid',
            get_string('mission', 'mod_labnbook'),
            ['0' => "-"],
            $attrs
        );
        $mform->addElement($dynamicselect);
        $mform->setType('labnbook_missionid', PARAM_INT);
        $mform->addRule('labnbook_missionid', null, 'required', null, 'client');

        $fetchmissionsdata = $fetcher->getmissionstoken();
        $PAGE->requires->js_call_amd('mod_labnbook/form', 'init', [
            $fetchmissionsdata[0],
            $fetchmissionsdata[1],
        ]);

        // Adding the "general" fieldset, where all the common settings are showed.

        $course = $this->get_course();
        $groups = groups_get_all_groups($course->id, 0, $course->defaultgroupingid);
        $groupoptions = ["error" => "-"];
        $groupoptions[0] = get_string('allparticipants');
        if (empty($groups)) {
            unset($groupoptions["error"]);
        }
        foreach ($groups as $g) {
            $groupoptions[$g->id] = $g->name;
        }
        $mform->addElement('select', 'groupid', get_string('group_for_activity', 'mod_labnbook'), $groupoptions, $disabled);
        $mform->addHelpButton('groupid', 'group_for_activity', 'mod_labnbook');
        $mform->setType('groupid', PARAM_INT);
        $mform->addRule("groupid", get_string("group_is_required", 'mod_labnbook'), 'numeric', null, 'client');
        $mform->addRule("groupid", get_string("group_is_required", 'mod_labnbook'), 'required', null, 'client');

        $mform->addElement('hidden', 'labnbook_teamconfigid', false);
        $mform->setType('labnbook_teamconfigid', PARAM_INT);
        $mform->addElement('header', 'general', get_string('general', 'form'));
        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('instancename', 'mod_labnbook'), ['size' => '64']);
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'instancename', 'mod_labnbook');
        // Todo ajax that queries LNB for the number of participants in that group.

        $this->standard_intro_elements();

        // Add standard grading elements.
        $this->standard_grading_coursemodule_elements();

        // Add standard elements.
        $this->standard_coursemodule_elements();

        // Add standard buttons.
        $this->add_action_buttons();
    }

    /**
     * Allows module to modify data returned by get_moduleinfo_data() or prepare_new_moduleinfo_data() before calling set_data()
     * This method is also called in the bulk activity completion form.
     *
     * Only available on moodleform_mod.
     *
     * @param array $defaultvalues passed by reference
     */
    public function data_preprocessing(&$defaultvalues) {
        parent::data_preprocessing($defaultvalues);
        // The labnbook missionid will not be correctly set as option list is retrieved by ajax.
        // Thus we store the id and let the ajax use it later.
        if (array_key_exists('labnbook_missionid', $defaultvalues)) {
            echo '<script>window.missionid=' . $defaultvalues['labnbook_missionid'] . ';</script>';
            // Retrieve teamconfig data.
            $fetcher = new fetch\User();
            try {
                $teamconfig = $fetcher->getteamconfig($defaultvalues['labnbook_teamconfigid']);
                foreach (['method', 'size_min', 'size_max', 'size_opt', 'teams_max'] as $field) {
                    $defaultvalues[$field] = $teamconfig->$field;
                }
            } catch (Exception $e) {
                // Ignore silently this error if teamconfig does not exists.
                return;
            }
        }
    }
}
