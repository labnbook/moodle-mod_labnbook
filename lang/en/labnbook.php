<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     mod_labnbook
 * @category    string
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['labnbook:addinstance'] = 'New labnbook activity';
$string['labnbook:edit'] = 'Edit labnbook';
$string['labnbook:view'] = 'View labnbook';
$string['missingidandcmid'] = 'Missing id and cmid';
$string['modulename'] = 'LabNBook';
$string['modulename_help'] = "This Moodle activity will grant an access to a LabNBook mission. After selecting the mission, you will have to configure how the teams of students will be built.";
$string['modulenameplural'] = 'LabNBook activities';
$string['newmodulefieldset'] = 'LabNBook';
$string['newmodulename'] = 'New LabNBook activity';
$string['newmodulename_help'] = 'Create a new activity matched with a LabNBook mission.';
$string['newmodulesettings'] = 'Settings';
$string['nonewmodules'] = 'No newmodules';
$string['pluginadministration'] = 'LabNBook administration';
$string['pluginname'] = 'LabNBook';
$string['privacy:metadata'] = 'The LabNBook moodle plugin does not store any personal data, but it does transfer the following informations to the LabNBook configured serveur for user binding their accounts : first and last name, login, email, student number (if provided).';
$string['view'] = 'View';
$string['labnbookSettings'] = 'Paramètres LabNBook';

// Settings.
$string['labnbook_api_url'] = "LabNBook URL";
$string['labnbook_api_url_descr'] = "The URL to the root of the labnbook API, with a trailing slash. E.g. https://uga.labnbook.fr/api/";
$string['labnbook_api_key'] = "LabNBook Key";
$string['labnbook_api_key_descr'] = "The secret key that grants Moodle an access to the API of LabNBook. This key is provided by the LabNBook instance.";
$string['labnbook_institution_id'] = "LabNBook external platform ID";
$string['labnbook_institution_id_descr'] = "The LabNBook external platform ID that will be assigned to this Moodle instance.";
$string['labnbook_send_user_email'] = "Send users email to LabNBook.";
$string['labnbook_send_user_email_descr'] = "This field is used by LabNBook to identify users and avoid duplicate accounts. It also enables teachers to send group emails directly from LabNBook.";
$string['labnbook_send_user_student_num'] = "Send user student numbers to LabNBook.";
$string['labnbook_send_user_student_num_descr'] = "This field is used by LabNBook to identify user reducing the risk of duplicated accounts.";

// Form.
$string['instancename'] = "Header";
$string['instancename_help'] = "This text will be shown as the activity title in Moodle.";
$string['mission'] = "Mission";
$string['addMission'] = "Create a new mission";
$string['addMission_help'] = "This button takes you to the LabNBook mission creation tab. Once the mission is created, hit the 'Refresh mission list' button";
$string['mission_help'] = "Select a mission among those you own in LabNBook. If you create a new mission in LabNBook, the 'refresh' button will update the drop-down list.";
$string['refresh'] = "Refresh mission list";
$string['refresh_help'] = "Refresh available mission list by querying LabNBook platform";
$string['teaming'] = "Team settings";
$string['method'] = "Method";
$string['method_help'] = "With the first choice, students will chose their own team if they don't have any.\n\nWith the second choice, they will be randomly assigned to a new or existing team when they begin their activity.\n\nWith the third choice, teams can only be managed by teachers from LabNBook.\n\nThe number of teams and their sizes are controlled by the next settings.";
$string['teamconfigmethod_students'] = "Students' choice";
$string['teamconfigmethod_random'] = "Random distribution";
$string['teamconfigmethod_teacher'] = "Teacher choice (managed in LabNBook)";
$string['teams_max'] = "Max number of teams";
$string['teams_max_help'] = "There will never be more than this number of teams, unless a teacher manually creates more teams.";
$string['size_opt'] = "Optimal size";
$string['size_opt_help'] = "The team size that would be optimal. The maximum and minimum sizes will be inferred from this value if they are not given.";
$string['size_max'] = "Maximal size";
$string['size_min'] = "Minimal size";
$string['group_for_activity'] = "Group for the activity";
$string['group_for_activity_help'] = "This options decides which student can access the mission in LabNBook - ti be used in conjonction with some access restriction below if you whish to hide the link in the activity list to the students that ca access the activity.";
$string['group_is_required'] = "You must choose which students can access this activity";
$string['field_required_for_method'] = "This field must be >0 for this teaming method";

// View as teacher.
$string['external_mission'] = "Edit the mission";
$string['external_teaming'] = "Choose the teaming options in LabNBook";
$string['external_reports'] = "View and manage reports produced by students";
$string['group_name'] = 'Group: {$a}';
$string['restricted_to_group'] = 'Access restricted to group "{$a}"';
$string['team_config_init'] = 'You now need to configure teaming in LabNBook. If you are not automatically redirected, click on the "Choose LabNBook teaming options"';
$string['reports_started'] = '{$a} started report';
$string['reports_started_plural'] = 'currently, {$a} started reports';
$string['reports_submitted'] = '{$a} of which is submitted';
$string['reports_submitted_plural'] = '{$a} of which were submitted';
$string['lnb_management_links'] = "Other LabNBook links for this activity:";
$string['lnb_management_main_links'] = "Finalization of the activity configuration on LabNBook:";
$string['external_classe'] = "See the LabNBook class used";
// The three following are printed in this order.
$string['enrolled_users_lnb'] = 'currently, {$a} student have already linked their Moodle account to LabNBook';
$string['enrolled_users_lnb_plural'] = 'currently, {$a} students have already linked their Moodle account to LabNBook';
$string['enrolled_users_on'] = ' on the ';
$string['enrolled_users_moodle'] = '{$a} student enrolled in this activity';
$string['enrolled_users_moodle_plural'] = '{$a} students enrolled in this activity';

// View as student.
$string['gotoreport'] = "Access to the report";
$string['error_duplicate_teaming'] = "This mission is already attributed to this group in this course. It is not possible to attribute the same mission to a same group in course several times. If you've just removed a similar activity, please wait a few minutes fro Moodle to refresh itself";
$string['not_part_of_group'] = "Cette activité est restreinte à un groupe auquel vous n'appartenez pas";

// View for binding.
$string['connectaccount'] = "Bind your Moodle account to LabNBook";
$string['connectaccountdetails'] = "In order to use LabNBook from Moodle, a link must be established between Moodle and a LabNBook account (existing or to be created).";
$string['informationtransmitted'] = "The following data will be sent to LabNBook:";
$string['bindaccount'] = "Bind my account";
$string['studentnumber'] = "Student number (if defined)";
$string['login'] = "Login";
$string['firstnameandlastname'] = "First and last name";
$string['onclickbind'] = "By clicking on the button below, a LabNBook tab will be opened, where you can make the connection.";
$string['emailifdefined'] = "E-mail address (if defined)";

// View for deleting activity.
$string['deleteActivityTitle'] = "Deleting a LabNBook activity";
$string['deleteActivityWithReport'] = "Caution ! Some student are already working on report from this activity ! You cannot delete this activity without removing the reports on LabNBook first.";
$string['cannotDeleteActivityTitle'] = "Cannot delete the LabNBook activity.";


// Privacy.
$string['privacy:metadata:labnbook'] = "In order to identify Moodle users in LabNbook a few user data are sent to LabNBook for user who bound their accounts.";
$string['privacy:metadata:labnbook:username'] = "Login of the user binding their account to LabNBook";
$string['privacy:metadata:labnbook:firstname'] = "Firstname of the user binding their account to LabNBook";
$string['privacy:metadata:labnbook:lastname'] = "Lastname of the user binding their account to LabNBook";
$string['privacy:metadata:labnbook:idnumber'] = "Student number of the user binding their account to LabNBook";
$string['privacy:metadata:labnbook:email'] = "Email of the user binding their account to LabNBook";
$string['privacy:reports'] = "Reports";
$string['privacy:labdocs'] = "Labdocs";
$string['privacy:comments'] = "Comments";
$string['privacy:conversations'] = "Conversations";
$string['privacy:messages'] = "Messages";
