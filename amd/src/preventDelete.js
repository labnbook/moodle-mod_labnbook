import ModalFactory from 'core/modal_factory';
import ModalEvents from 'core/modal_events';

export const init = async(title, message, courseid) => {
    const modal = await ModalFactory.create({
        type: ModalFactory.types.CANCEL,
        title: title,
        body: message,
    });
    await modal.show();
    modal.getRoot().on(ModalEvents.hidden, function() {
        window.location = '/course/view.php?id=' + courseid;
    });
};
