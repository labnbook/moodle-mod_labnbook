export const init = (url, token) => {
    var labnbookMissions = new Map();

    /**
     * Fetches a mission by it's id
     * @param {int} id
     */
    function getMission(id) {
        return labnbookMissions.get(parseInt(id));
    }

    var refreshLock = 0;

    /**
     * Marks an element as loading
     * @param {string} selector querySelector
     */
    function setLoading(selector) {
        const elem = document.querySelector(selector);
        if (elem) {
            const loading = document.createElement('span');
            loading.setAttribute('class', 'fa fa-spinner fa-pulse fa-fw labnbook-missions-loading');
            elem.parentNode.appendChild(loading);
        }
    }

    /**
     * Marks an element as loading
     */
    function unSetLoading() {
        const elems = document.querySelectorAll('.labnbook-missions-loading');
        for (let i = 0; i < elems.length; i++) {
            elems[i].remove();
        }
    }

    /**
     * Refreshes the mission list
     */
    function refreshMissions() {
        if (refreshLock) {
            return;
        }
        refreshLock = 1;

        setLoading('#refresh-missions');
        setLoading('#id_labnbook_missionid');

        fetch(url, {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'omit',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
            mode: 'cors',
            referrer: 'no-referrer',
        }).then(function(r) {
            if (r.ok) {
                return r.json();
            } else {
                window.console.error(r);
                throw new Error(r);
            }
        }).then(x => x.data).then(missions => {
            refreshLock = 0;
            unSetLoading();
            updateMissionsHTML(missions);
            return true;
        }).catch((e) => {
            refreshLock = 0;
            unSetLoading();
            alert(e);
        });
    }

    /**
     * Shows or hide addMission and refresh buttons
     * @param {bool} show
     */
    function showHideAddMissionAndRefresh(show) {
        var mainContainer = document.querySelector('#fitem_id_labnbook_missionid');
        var addMission = document.querySelector('#fitem_id_addMission');
        var refresh = document.querySelector('#fitem_id_refresh');
        var otherFieldsets = document.querySelectorAll('fieldset:not(#id_external)');
        var errorDiv = document.querySelector('.fdescription');

        // Display type when we should show create and refresh buttons
        var displayFlexNone = show ? 'flex' : 'none';
        var displayNoneBlock = show ? 'none' : 'block';
        var displayNoneFlex = show ? 'none' : 'flex';


        if (addMission) {
            addMission.style.display = displayFlexNone;
            mainContainer.style.display = displayNoneFlex;
        }
        if (refresh) {
            refresh.style.display = displayFlexNone;
        }
        otherFieldsets.forEach((el) => {
            el.style.display = displayNoneBlock;
        });
        errorDiv.style.display = displayNoneBlock;
    }

    /**
     * Update the mission list html
     * @param {object} missions
     */
    function updateMissionsHTML(missions) {
        var container = document.querySelector('select[name="labnbook_missionid"]');
        container.innerHTML = '<option value="0">-</option>';
        showHideAddMissionAndRefresh(missions.length == 0);
        for (let m of missions) {
            labnbookMissions.set(m.id_mission, m);
            var e = document.createElement('option');
            e.setAttribute('value', m.id_mission);
            e.innerText = '[' + m.code + '] ' + m.name;
            container.appendChild(e);
        }
        if (typeof (window.missionid) != 'undefined') {
            document.querySelector('select[name="labnbook_missionid"]').value = window.missionid;
        }
    }

    /**
     * Makes readonly selector disabled
     */
    function disabledReadonlySelectors() {
        document.querySelectorAll('select[readonly]').forEach(
            function(selector) {
                selector.style.cssText = "opacity:0.6";
                for (let i = 0; i < selector.options.length; i++) {
                    let option = selector.options[i];
                    if (!option.selected) {
                        selector.removeChild(option);
                        i = i - 1;
                    }
                }
            });
    }

    showHideAddMissionAndRefresh(false);
    var submitButton = document.querySelector('#id_submitbutton2');
    submitButton.parentNode.removeChild(submitButton);
    refreshMissions();
    disabledReadonlySelectors();
    window.refreshMissions = refreshMissions;
    window.getMission = getMission;
};
