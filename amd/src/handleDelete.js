import ModalFactory from 'core/modal_factory';
import {activityHasStartedReports} from './repository';

export const init = (title, message) => {
    /**
     * Handles a delete request on a LabNbook activity.
     * @param {Event} evt
     * @param {DOMElement} elem
     */
    async function deleteHandler(evt, elem) {
        let shouldPrevent = elem.getAttribute('data-prevent-delete');
        if (shouldPrevent !== "false") {
            evt.preventDefault();
            evt.stopPropagation();
            let activityID = elem.getAttribute('data-id');
            if (!activityID) {
                activityID = new URLSearchParams(elem.getAttribute('href')).get('delete');
            }
            const hasStartedReports = await activityHasStartedReports(activityID);
            if (hasStartedReports) {
                const modal = await ModalFactory.create({
                    type: ModalFactory.types.CANCEL,
                    title: title,
                    body: message
                });
                await modal.show();
            } else {
                elem.setAttribute('data-prevent-delete', false);
                elem.dispatchEvent(evt);
            }
        } else {
            elem.setAttribute('data-prevent-delete', true);
        }
    }
    document.querySelectorAll('li.labnbook .editing_delete').forEach(function(elem) {
         elem.addEventListener('click', async function(evt) {
             deleteHandler(evt, elem);
         });
     });
};
