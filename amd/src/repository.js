import {call as fetchMany} from 'core/ajax';

export const activityHasStartedReports = (
    activityid,
) => fetchMany([{
    methodname: 'mod_labnbook_activity_has_started_reports',
    args: {
        activityid,
    },
}])[0];
