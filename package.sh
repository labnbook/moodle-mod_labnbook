#!/bin/bash

cd ..
rm labnbook.zip;
zip -r labnbook.zip labnbook/  -x '*/.git*' -x 'vendor*' -x '*.*.sw?' -x '*/lang/fr*' -x 'labnbook/patch' -x 'labnbook/package.sh'
