<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin upgrade steps are defined here.
 *
 * @category    upgrade
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */

/**
 * Execute mod_labnbook upgrade from the given old version.
 *
 * @param int $oldversion
 * @return bool
 * @package mod_labnbook
 */
function xmldb_labnbook_upgrade($oldversion) {
    global $DB, $CFG;

    $dbman = $DB->get_manager();
    if ($oldversion < 2021010500) {
        // Rename field course on table labnbook to NEWNAMEGOESHERE.
        $table = new xmldb_table('labnbook');
        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'id');

        // Launch rename field course.
        $dbman->rename_field($table, $field, 'course');
        upgrade_mod_savepoint(true, 2021010500, 'labnbook');
    }
    if ($oldversion < 2021051900) {
        $table = new xmldb_table('labnbook');
        $field = new xmldb_field('grade', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, '0', null);
        $dbman->add_field($table, $field);
        upgrade_mod_savepoint(true, 2021051900, 'labnbook');
    }

    if ($oldversion < 2021120118) {
        $table = new xmldb_table('labnbook');
        $field = new xmldb_field('groupid', XMLDB_TYPE_INTEGER, '11', null, null, null, '0', null);
        $dbman->add_field($table, $field);
        upgrade_mod_savepoint(true, 2021120118, 'labnbook');
    }

    if ($oldversion < 2024082200) {
        $roleids = $DB->get_records_menu('role', null, '', 'shortname, id');
        if (isset($roleids['teacher'])) {
            $systemcontext = context_system::instance();
            assign_capability('mod/labnbook:view', CAP_ALLOW, $roleids['teacher'], $systemcontext->id);
        }

        if (!empty($CFG->labnbook_api_url) && empty(get_config('labnbook', 'api_url'))) {
            set_config('api_url', $CFG->labnbook_api_url, 'labnbook');
            set_config('api_key', $CFG->labnbook_api_key, 'labnbook');
            set_config('institution_id', $CFG->labnbook_institution_id, 'labnbook');
        }

        upgrade_mod_savepoint(true, 2024082200, 'labnbook');
    }

    // For further information please read the Upgrade API documentation:
    // At https://docs.moodle.org/dev/Upgrade_API .

    return true;
}
