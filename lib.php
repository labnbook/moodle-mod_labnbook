<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */
use mod_labnbook\local\fetch;
use mod_labnbook\local\fetch\user as fetchUser;
use mod_labnbook\local\helper;

define('LABNBOOK_TABLE', 'labnbook');


/** @var $DB moodle_database */

/**
 * Calls get_string pluralizing the string if the number of elements is > 1
 * @param string $name string name
 * @param string $from module name
 * @param int $a number of element
 * @return string
 */
function mod_labnbook_get_string_plural($name, $from, $a) {
    $plural = 1 === (int)$a ? '' : '_plural';
    return get_string($name . $plural, $from, $a);
}

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature constant representing the feature
 * @return true | null True if the feature is supported, null otherwise
 * @package mod_labnbook
 */
function labnbook_supports($feature) {
    $features = [
        FEATURE_GROUPS,
        FEATURE_MOD_INTRO,
        FEATURE_SHOW_DESCRIPTION,
        FEATURE_BACKUP_MOODLE2,
    ];
    return in_array($feature, $features) ? true : null;
}

/**
 * Returns the different group, class and activity names as an array
 * @param object $course
 * @param object $moduleinstance
 * @return array
 * @package mod_labnbook
 */
function mod_labnbook_get_activity_names($course, $moduleinstance) {
    $coursename = $course->shortname;
    $groupname = $moduleinstance->groupid ? groups_get_group_name($moduleinstance->groupid) : get_string("allparticipants");
    // Name can be '[code] name (groupname)' or '[code] name' or '[code] name - restrictions'.
    $name = preg_replace("/\[.*\]$| - .*$|\(Groupe?.*\)$/", "", $moduleinstance->name);
    $displayname = $moduleinstance->groupid ? ($name . ' - ' . get_string("restricted_to_group", "labnbook", $groupname)) : $name;
    // Filter to get the actual name.
    return [
        "class" => $coursename . " (" . get_string('group_name', 'labnbook', $groupname) . ")",
        "display_name" => $displayname,
    ];
}

/**
 * Saves a new instance of the mod_labnbook into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $moduleinstance an object from the form
 * @param mod_labnbook_mod_form $mform the form
 * @return int the id of the newly inserted record
 * @package mod_labnbook
 */
function labnbook_add_instance($moduleinstance, $mform = null) {
    global $DB;
    $course = $mform->get_course();
    $fetcher = new fetch\user();
    $teamconfig = [
        'end_datetime' => mod_labnbook_get_end_date($moduleinstance),
    ];
    $names = mod_labnbook_get_activity_names($course, $moduleinstance);
    try {
        $teamconfig = $fetcher->usemission(
            (int) $moduleinstance->labnbook_missionid,
            (int) $moduleinstance->course,
            $names['class'],
            (int) $moduleinstance->groupid,
            get_participants_ids($course, $moduleinstance),
            $teamconfig
        );
    } catch (Exception $e) {
        $error = $fetcher->getlasterror();
        if ($error->code == 409) {
            throw new moodle_exception(get_string('error_duplicate_teaming', 'labnbook'));
        } else {
            die("fatal error: " . $error->message);
        }
    }
    if ($teamconfig->id_team_config) {
        $moduleinstance->labnbook_teamconfigid = (int) $teamconfig->id_team_config;
    } else {
        throw new moodle_exception("LabnBook return an invalid response.");
    }

    $moduleinstance->timecreated = time();
    $moduleinstance->name = $names['display_name'];
    $id = $DB->insert_record(LABNBOOK_TABLE, $moduleinstance);
    $_SESSION['labnbook_add'] = $id;

    return $id;
}

/**
 * Returns the end date of a module instance
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @return date|null
 * @package mod_labnbook
 */
function mod_labnbook_get_end_date($moduleinstance) {
    if (property_exists($moduleinstance, 'id')) {
        $cm = get_coursemodule_from_instance("labnbook", $moduleinstance->id, $moduleinstance->course, false, MUST_EXIST);
    } else {
        // While not created the module instance holds cm infos.
        $cm = $moduleinstance;
    }

    return $cm && $cm->completionexpected ? date('c', $cm->completionexpected) : null;
}

/**
 * Returns the ids of members of the given course
 * @param Course $course
 * @param Moduleinstance $moduleinstance
 * @return array
 */
function get_participants_ids($course, $moduleinstance) {
    $context = context_course::instance($course->id);

    /**
     * Returns the participant id of a participant
     * @param stdClass $p
     * @return int
     */
    function participants_to_id($p) {
        return $p->id;
    }
    $allusers = (array)array_map('participants_to_id', get_enrolled_users($context, '', $moduleinstance->groupid, 'u.id'));
    // Do not consider editing teachers as participants i.e students.
    $teachers = (array)array_map(
        'participants_to_id',
        get_enrolled_users($context, 'moodle/course:update', $moduleinstance->groupid, 'u.id')
    );
    return array_diff($allusers, $teachers);
}

/**
 * Updates an instance of the mod_labnbook in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_labnbook_mod_form $mform the form
 * @return bool true if successful, false otherwise
 * @package mod_labnbook
 */
function labnbook_update_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    // Prevent manual change of groupid.
    $oldinstance = $DB->get_record('labnbook', ['id' => $moduleinstance->id]);
    $moduleinstance->groupid = $oldinstance->groupid;

    $fetcher = new fetch\user();
    $names = mod_labnbook_get_activity_names($mform->get_course(), $moduleinstance);
    $moduleinstance->name = $names['display_name'];

    return $DB->update_record(LABNBOOK_TABLE, $moduleinstance);
}

/**
 * Removes an instance of the mod_labnbook from the database.
 *
 * @param int $id id of the module instance
 * @return bool true if successful, false on failure
 * @package mod_labnbook
 */
function labnbook_delete_instance($id) {
    global $DB;

    $record = $DB->get_record(LABNBOOK_TABLE, ['id' => $id]);
    if (!$record) {
        return true;
    }
    try {
        $fetcher = new fetch\institution();
        $fetcher->deleteteamconfig($record->labnbook_teamconfigid);
    } catch (Exception $e) {
        $error = $fetcher->getlasterror();
        // 404 means already removed in this case, we just ignore the error
        if (isset($error->code) && $error->code != 404) {
            throw $e;
        }
    }
    return $DB->delete_records(LABNBOOK_TABLE, ['id' => $id]);
}

/**
 * Given a course_module object, this function returns any
 * "extra" information that may be needed when printing
 * this activity in a course listing.
 *
 * See {@see get_array_of_activities()} in course/lib.php
 *
 * @param stdClass $coursemodule
 * @return cached_cm_info Info to customise main page display
 * @package mod_labnbook
 */
function labnbook_get_coursemodule_info($coursemodule) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");

    $instance = $DB->get_record(
        LABNBOOK_TABLE,
        ['id' => $coursemodule->instance],
        'id, name, intro, introformat'
    );
    if (!$instance) {
        return null;
    }

    $info = new cached_cm_info();
    $info->name = $instance->name;
    if ($coursemodule->showdescription) {
        // Convert intro to html. Do not filter cached version, filters run at display time.
        $info->content = format_module_intro('labnbook', $instance, $coursemodule->id, false);
    }

    return $info;
}

/**
 * Is a given scale used by the instance of mod_labnbook?
 *
 * This function returns if a scale is being used by one mod_labnbook
 * if it has support for grading and scales.
 *
 * @param int $moduleinstanceid ID of an instance of this module
 * @param int $scaleid ID of the scale
 * @return bool true if the scale is used by the given mod_labnbook instance
 * @package mod_labnbook
 */
function labnbook_scale_used($moduleinstanceid, $scaleid) {
    global $DB;

    if ($scaleid && $DB->record_exists(LABNBOOK_TABLE, ['id' => $moduleinstanceid, 'grade' => -$scaleid])) {
        return true;
    }
    return false;
}

/**
 * Checks if scale is being used by any instance of mod_labnbook.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param int $scaleid ID of the scale
 * @return bool true if the scale is used by any mod_labnbook instance
 * @package mod_labnbook
 */
function labnbook_scale_used_anywhere($scaleid) {
    global $DB;

    if ($scaleid && $DB->record_exists(LABNBOOK_TABLE, ['grade' => -$scaleid])) {
        return true;
    }
    return false;
}

/**
 * Creates or updates grade item for the given mod_labnbook instance.
 *
 * Needed by {@see grade_update_mod_grades()}.
 *
 * @param stdClass $moduleinstance instance object with extra cmidnumber and modname property
 * @param bool $reset reset grades in the gradebook
 * @return void
 * @package mod_labnbook
 */
function labnbook_grade_item_update($moduleinstance, $reset = false) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    $item = [];
    $item['itemname'] = clean_param($moduleinstance->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;

    if ($moduleinstance->grade > 0) {
        $item['gradetype'] = GRADE_TYPE_VALUE;
        $item['grademax']  = $moduleinstance->grade;
        $item['grademin']  = 0;
    } else if ($moduleinstance->grade < 0) {
        $item['gradetype'] = GRADE_TYPE_SCALE;
        $item['scaleid']   = -$moduleinstance->grade;
    } else {
        $item['gradetype'] = GRADE_TYPE_NONE;
    }
    if ($reset) {
        $item['reset'] = true;
    }

    grade_update('/mod/labnbook', $moduleinstance->course, 'mod', LABNBOOK_TABLE, $moduleinstance->id, 0, null, $item);
}

/**
 * Delete grade item for given mod_labnbook instance.
 *
 * @param stdClass $moduleinstance instance object
 * @return grade_item
 * @package mod_labnbook
 */
function labnbook_grade_item_delete($moduleinstance) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    return grade_update(
        '/mod/labnbook',
        $moduleinstance->course,
        'mod',
        LABNBOOK_TABLE,
        $moduleinstance->id,
        0,
        null,
        ['deleted' => 1]
    );
}

/**
 * Update mod_labnbook grades in the gradebook.
 *
 * Needed by {@see grade_update_mod_grades()}.
 *
 * @param stdClass $moduleinstance instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 * @package mod_labnbook
 */
function labnbook_update_grades($moduleinstance, $userid = 0) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    // Populate array of grade objects indexed by userid.
    $grades = [];
    grade_update('/mod/labnbook', $moduleinstance->course, 'mod', LABNBOOK_TABLE, $moduleinstance->id, 0, $grades);
}

/**
 * Extends the global navigation tree by adding mod_labnbook nodes if there is a relevant content.
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $labnbooknode an object representing the navigation tree node
 * @param stdClass $course
 * @param stdClass $module
 * @param cm_info $cm
 * @package mod_labnbook
 */
function labnbook_extend_navigation($labnbooknode, $course, $module, $cm) {
}

/**
 * Extends the settings navigation with the mod_labnbook settings.
 *
 * This function is called when the context for the page is a mod_labnbook module.
 * This is not called by AJAX so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@see settings_navigation}
 * @param navigation_node $labnbooknode {@see navigation_node}
 * @package mod_labnbook
 */
function labnbook_extend_settings_navigation($settingsnav, $labnbooknode = null) {
}

/**
 * Avoid unauthorized delete of labnbook activities
 * @package mod_labnbook
 */
function labnbook_before_footer() {
    global $PAGE, $DB;
    if ($PAGE->url->get_path() === '/course/view.php') {
        $message = get_string('deleteActivityWithReport', 'mod_labnbook');
        $title = get_string('deleteActivityTitle', 'mod_labnbook');
        $PAGE->requires->js_call_amd(
            'mod_labnbook/handleDelete',
            'init',
            [$title, $message]
        );
    }
    if ($PAGE->url->get_path() === '/course/mod.php') {
        $activityid = $PAGE->url->get_param('delete');
        if ($activityid != null) {
            $moduleinstance = $DB->get_record(LABNBOOK_TABLE, ['id' => $PAGE->cm->instance], '*', MUST_EXIST);
            helper::requireLabnbookAuthentication();
            $fetcher = new fetchUser();
            if ($fetcher->getnumstartedreports($moduleinstance->labnbook_teamconfigid) > 0) {
                $title = get_string('cannotDeleteActivityTitle', 'mod_labnbook');
                $message = get_string('deleteActivityWithReport', 'mod_labnbook');
                $courseid = $PAGE->course->id;
                $PAGE->requires->js_call_amd(
                    'mod_labnbook/preventDelete',
                    'init',
                    [$title, $message, $courseid]
                );
            }
        }
    }
}
