<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays an instance of mod_labnbook according to the user's permission.
 *
 * @copyright   2019 Université Grenoble Alpes
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package mod_labnbook
 */

use mod_labnbook\local\fetch\user as fetchUser;
use mod_labnbook\local\helper;

require(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/lib.php');

/**
 * renders the templates and stop processing the request
 * @param string $templatename
 * @param array $templatedata
 */
function renderanddie($templatename, $templatedata) {
    global $PAGE, $OUTPUT, $moduleinstance, $course, $modulecontext, $cm;
    $PAGE->set_url('/mod/labnbook/view.php', ['id' => $cm->id]);
    $PAGE->set_title(format_string($moduleinstance->name));
    $PAGE->set_heading(format_string($course->fullname));
    $PAGE->set_context($modulecontext);

    echo $OUTPUT->header();
    echo $OUTPUT->render_from_template("mod_labnbook/$templatename", $templatedata);
    echo $OUTPUT->footer();
    die();
}

// Course_module ID, or ?
$id = optional_param('id', 0, PARAM_INT);

// ... module instance id.
$l  = optional_param('l', 0, PARAM_INT);

if ($id) {
    $cm             = get_coursemodule_from_id("labnbook", $id, 0, false, MUST_EXIST);
    $course         = $DB->get_record('course', ['id' => $cm->course], '*', MUST_EXIST);
    $moduleinstance = $DB->get_record(LABNBOOK_TABLE, ['id' => $cm->instance], '*', MUST_EXIST);
    unset($id);
} else if ($l) {
    $moduleinstance = $DB->get_record(LABNBOOK_TABLE, ['id' => $l], '*', MUST_EXIST);
    $course         = $DB->get_record('course', ['id' => $moduleinstance->course], '*', MUST_EXIST);
    $cm             = get_coursemodule_from_instance("labnbook", $moduleinstance->id, $course->id, false, MUST_EXIST);
    unset($l);
} else {
    moodle_exception(get_string('missingidandcmid', mod_labnbook));
}

require_login($course, true, $cm);

$modulecontext = context_module::instance($cm->id);
$coursecontext = context_course::instance($course->id);

// The teacher has access to all the missions and classes used in this course.
$isteacher = has_capability('moodle/course:manageactivities', $coursecontext)
    || has_capability('mod/labnbook:grade', $coursecontext);


$templatedata = [
    'title' => $moduleinstance->name,
    'description' => format_text($moduleinstance->intro, $moduleinstance->introformat, ['context' => $modulecontext]),
];
if (!$isteacher) {
    // Retrieve user groups.
    $groupingid = $cm->groupmode && $cm->groupingid ? $cm->groupingid : $course->defaultgroupingid;
    // Get all groups of the user for this course and the allowed groupingid if defined.
    $groups = groups_get_all_groups($course->id, $USER->id, $groupingid);
    if ($moduleinstance->groupid > 0 && !array_key_exists($moduleinstance->groupid, $groups)) {
        $templatedata['back'] = get_string('back');
        $templatedata['error'] = get_string('not_part_of_group', 'labnbook');
        renderanddie('view_error', $templatedata);
    }
}

$destination = helper::getactivitylocalurlforredirect($moduleinstance, $isteacher);

helper::requireLabnbookAuthentication($destination);

$fetcher = new fetchUser();
$templatedata['redirecturl'] = $fetcher->getredirecturl();
if ($isteacher) {
    helper::grantteacheraccess();
    $participants = get_participants_ids($course, $moduleinstance);
    $participantslinkedcount = $fetcher->updateparticipants($course->id, $moduleinstance->groupid, $participants);
    $templatedata['enrolled_users'] = mod_labnbook_get_string_plural('enrolled_users_lnb', 'labnbook', $participantslinkedcount)
        . get_string('enrolled_users_on', 'labnbook')
        . mod_labnbook_get_string_plural('enrolled_users_moodle', 'labnbook', count($participants));
    // Editing teacher view.
    $templatename = 'view_teacher';
    $templatedata['token_teaming'] = $fetcher->getteamingtokenforredirect($moduleinstance->labnbook_teamconfigid);
    $templatedata['token_reports'] = $fetcher->getreportstokenforredirect($moduleinstance->labnbook_teamconfigid);
    $templatedata['token_mission'] = $fetcher->getMissionedittokenforredirect($moduleinstance->labnbook_missionid);
    $templatedata['token_classe'] = $fetcher->getclassedittokenforredirect($moduleinstance->labnbook_teamconfigid);
    $templatedata['started_reports'] = mod_labnbook_get_string_plural(
        'reports_started',
        'labnbook',
        $fetcher->getnumstartedreports($moduleinstance->labnbook_teamconfigid)
    );
    $submitted = $fetcher->getnumsubmittedreports($moduleinstance->labnbook_teamconfigid);
    $templatedata['submitted_reports'] = $submitted ?
        mod_labnbook_get_string_plural('reports_submitted', 'labnbook', $submitted) :
        "";

    $templatedata['open_team_config'] = (isset($_SESSION['labnbook_add']) && $_SESSION['labnbook_add'] == $moduleinstance->id);
    unset($_SESSION['labnbook_add']);
    $templatedata['open_team_config_msg'] = addslashes(get_string('team_config_init', 'labnbook'));
    $templatedata['main_links_text'] = get_string('lnb_management_main_links', 'labnbook');
    $templatedata['other_links_text'] = get_string('lnb_management_links', 'labnbook');
} else {
    // Student view.
    $templatename = 'view_student';
    $templatedata['token_report'] = $fetcher->getreporttokenforredirect($moduleinstance->labnbook_teamconfigid);
}
renderanddie($templatename, $templatedata);
