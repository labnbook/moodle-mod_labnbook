<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides support for backup LabNBook activities in the moodle2 backup format
 *
 * @package mod_labnbook
 * @copyright   2024 Université Grenoble Alpes
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * Povide structure of LabNBook data
 *
 * @copyright   2024 Université Grenoble Alpes
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @see backup_activity_structure_step
 */
class backup_labnbook_activity_structure_step extends backup_activity_structure_step {
    /**
     * @var array LabNBook table field list
     */
    protected const FIELDS = [
        'id',
        'course',
        'groupid',
        'name',
        'intro',
        'introformat',
        'labnbook_missionid',
        'labnbook_teamconfigid',
        'timecreated',
        'timemodified',
        'grade',
    ];

    /**
     * Define the full structure of a LabNBook instance with user data
     * {@inheritDoc}
     * @see backup_structure_step::define_structure()
     */
    protected function define_structure() {
        $idfield = ['id'];
        $labnbook = new backup_nested_element('labnbook', $idfield, self::FIELDS);
        $query = 'SELECT * FROM {labnbook}';
        $query .= '   WHERE id = ?';
        $labnbook->set_source_sql($query, [backup::VAR_ACTIVITYID]);
        $labnbook->annotate_files('mod_labnbook', 'intro', null);
        return $this->prepare_activity_structure($labnbook);
    }
}
