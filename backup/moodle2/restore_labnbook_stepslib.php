<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides support for restore LabNBook activities in the moodle2 backup format
 *
 * @package mod_labnbook
 * @copyright   2024 Université Grenoble Alpes
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_labnbook\local\fetch\user as fetchUser;
use mod_labnbook\local\helper;

/**
 * Povide structure of LabNBook data
 *
 * @copyright   2024 Université Grenoble Alpes
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @see backup_activity_structure_step
 */
class restore_labnbook_activity_structure_step extends restore_activity_structure_step {
    /**
     * {@inheritDoc}
     * @see restore_structure_step::define_structure()
     */
    protected function define_structure() {

        $paths = [];

        $paths[] = new restore_path_element('labnbook', '/activity/labnbook');

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }


    /**
     * Preprocess and restore a LabNBook instance
     * @param array $data
     */
    protected function process_labnbook($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $course = $DB->get_record('course', ['id' => $data->course], '*', MUST_EXIST);

        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // Restore or recreate teamconfig.
        helper::requireLabnbookAuthentication();
        $fetcher = new fetchUser();

        $teamconfig = [
            'id_team_config' => $data->labnbook_teamconfigid,
        ];
        $names = mod_labnbook_get_activity_names($course, $data);
        $teamconfig = $fetcher->usemission(
            (int) $data->labnbook_missionid,
            (int) $data->course,
            $names['class'],
            (int) $data->groupid,
            get_participants_ids($course, $data),
            $teamconfig,
        );
        $data->labnbook_teamconfigid = $teamconfig->id_team_config;

        // Insert the labnbook record.
        $newitemid = $DB->insert_record('labnbook', $data);
        // Immediately after inserting "activity" record, call this.
        $this->apply_activity_instance($newitemid);
    }

    /**
     * {@inheritDoc}
     * @see restore_structure_step::after_execute()
     */
    protected function after_execute() {
        // Add labnbook related files, no need to match by itemname (just internally handled context).
        $this->add_related_files('mod_labnbook', 'intro', null);
    }
}
