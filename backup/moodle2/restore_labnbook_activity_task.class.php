<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the backup activity for the LabNBook module
 *
 * @package mod_labnbook
 * @copyright   2024 Université Grenoble Alpes
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();
require_once(dirname(__FILE__) . '/restore_labnbook_stepslib.php');

/**
 * LabNBook restore task class that provides all the settings and steps to perform it
 *
 * @package mod_labnbook
 * @copyright   2024 Université Grenoble Alpes
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_labnbook_activity_task extends restore_activity_task {
    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        // No particular settings for this activity.
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        // LabNBook only has one structure step.
        $this->add_step(new restore_labnbook_activity_structure_step('labnbook_structure', 'labnbook.xml'));
    }

    /**
     * Define the contents in the activity that must be
     * processed by the link decoder
     */
    public static function define_decode_contents() {
        $contents = [];

        $contents[] = new restore_decode_content('labnbook', ['intro'], 'labnbook');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    public static function define_decode_rules() {
        $rules = [];

        $rules[] = new restore_decode_rule('LABNBOOKINDEX', '/mod/labnbook/index.php?id=$1', 'course');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * when restoring labnbook logs.
     */
    public static function define_restore_log_rules() {
        $rules = [];

        $rules[] = new restore_log_rule('labnbook', 'add', 'view.php?id={course_module}', '{labnbook}');
        $rules[] = new restore_log_rule('labnbook', 'update', 'view.php?id={course_module}', '{labnbook}');
        $rules[] = new restore_log_rule('labnbook', 'view', 'view.php?id={course_module}', '{labnbook}');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * when restoring course logs.
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    public static function define_restore_log_rules_for_course() {
        $rules = [];

        // Fix old wrong uses (missing extension).
        $rules[] = new restore_log_rule('labnbook', 'view all', 'index?id={course}', null, null, null, 'index.php?id={course}');
        $rules[] = new restore_log_rule('labnbook', 'view all', 'index.php?id={course}', null);

        return $rules;
    }
}
